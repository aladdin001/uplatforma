package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"net/http"
	"os"
	"time"
	"uplatforma/api/handlers"
	"uplatforma/app"
)

func openConnection() {

	var err error
	app.DB, err = gorm.Open("postgres", os.Getenv("API_DB_OPEN"))
	if err != nil {
		panic("failed to connect database:\n" + err.Error())
	}

	err = app.DB.DB().Ping()
	if err != nil {
		panic("failed to connect database:\n" + err.Error())
	}
}

func init() {
	openConnection()
}

func main() {

	router := gin.Default()
	router.GET("/companies/:limit/:offset", handlers.GetCompaniesList)
	router.GET("/company/:id", handlers.GetCompany)
	router.PUT("/company", handlers.CreateCompany)
	router.POST("/company/:id", handlers.UpdateCompany)
	router.DELETE("/company/:id", handlers.DeleteCompany)

	router.GET("/contracts/:limit/:offset", handlers.GetContractsList)
	router.GET("/contract/:id", handlers.GetContract)
	router.PUT("/contract", handlers.CreateContract)
	router.POST("/contract/:id", handlers.UpdateContract)
	router.DELETE("/contract/:id", handlers.DeleteContract)

	router.POST("/purchase", handlers.Purchase)

	s := &http.Server{
		Addr:         "localhost:8080",
		Handler:      router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	s.ListenAndServe()
}
