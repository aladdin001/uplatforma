package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"uplatforma/models"
	"uplatforma/repositories"
)

func GetCompaniesList(c *gin.Context) {

	limit, ok := c.Params.Get("limit")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: limit")
		return
	}

	offset, ok := c.Params.Get("offset")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: offset")
		return
	}

	l, err := strconv.Atoi(limit)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: limit")
		return
	}

	o, err := strconv.Atoi(offset)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: offset")
		return
	}

	companies, err := repositories.GetCompaniesList(l, o)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, companies)
}

func GetCompany(c *gin.Context) {

	id_param, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: id")
		return
	}

	id, err := strconv.Atoi(id_param)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: id")
		return
	}

	companies, err := repositories.GetCompanyById(id)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, companies)
}

func UpdateCompany(c *gin.Context) {

	id_param, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: id")
		return
	}

	id, err := strconv.Atoi(id_param)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: id")
		return
	}

	name := c.PostForm("name")
	if name == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: name")
		return
	}

	registrationCode := c.PostForm("regstration_code")
	if registrationCode == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: regstration_code")
		return
	}

	company := models.Company{
		ID:               id,
		Name:             name,
		RegistrationCode: registrationCode,
	}

	err = repositories.UpdateCompany(company)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, company)
}

func CreateCompany(c *gin.Context) {

	name := c.PostForm("name")
	fmt.Println(name)
	if name == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: name")
		return
	}

	registrationCode := c.PostForm("regstration_code")
	if registrationCode == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: regstration_code")
		return
	}

	company := models.Company{
		Name:             name,
		RegistrationCode: registrationCode,
	}

	err := repositories.UpdateCompany(company)
	fmt.Println(err)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, company)
}

func DeleteCompany(c *gin.Context) {

	id_param, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: id")
		return
	}

	id, err := strconv.Atoi(id_param)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: id")
		return
	}

	err = repositories.DeleteCompanyById(id)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, true)
}
