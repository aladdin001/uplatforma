package handlers

const (
	StatusOk    = "ok"
	StatusError = "error"
)

type Response struct {
	Status string      `json:"status"`
	Result interface{} `json:"result"`
}

var ResponseError struct {
	Error string `json:"error"`
}
