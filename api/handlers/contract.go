package handlers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"uplatforma/models"
	"uplatforma/repositories"
)

func GetContractsList(c *gin.Context) {

	limit, ok := c.Params.Get("limit")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: limit")
		return
	}

	offset, ok := c.Params.Get("offset")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: offset")
		return
	}

	l, err := strconv.Atoi(limit)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: limit")
		return
	}

	o, err := strconv.Atoi(offset)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: offset")
		return
	}

	Contracts, err := repositories.GetContractList(l, o)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, Contracts)
}

func GetContract(c *gin.Context) {

	id_param, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: id")
		return
	}

	id, err := strconv.Atoi(id_param)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: id")
		return
	}

	Contracts, err := repositories.GetContractById(id)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, Contracts)
}

func UpdateContract(c *gin.Context) {

	id_param, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: id")
		return
	}
	id, err := strconv.Atoi(id_param)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: id")
		return
	}

	sellerIdParam := c.PostForm("seller_id")
	if sellerIdParam == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: sellerId")
		return
	}
	sellerId, err := strconv.Atoi(sellerIdParam)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: sellerId")
		return
	}

	clientIdParam := c.PostForm("client_id")
	if sellerIdParam == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: clientId")
		return
	}
	clientId, err := strconv.Atoi(clientIdParam)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: clientId")
		return
	}

	contractNumber := c.PostForm("contract_number")
	if contractNumber == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: contractNumber")
		return
	}

	contract := models.Contract{
		ID:             id,
		SellerId:       sellerId,
		ClientId:       clientId,
		ContractNumber: contractNumber,
	}

	err = repositories.UpdateContract(contract)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, contract)
}

func CreateContract(c *gin.Context) {

	sellerIdParam := c.PostForm("seller_id")
	if sellerIdParam == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: sellerId")
		return
	}
	sellerId, err := strconv.Atoi(sellerIdParam)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: sellerId")
		return
	}

	clientIdParam := c.PostForm("client_id")
	if sellerIdParam == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: clientId")
		return
	}
	clientId, err := strconv.Atoi(clientIdParam)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: clientId")
		return
	}

	contractNumber := c.PostForm("contract_number")
	if contractNumber == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: contractNumber")
		return
	}

	contract := models.Contract{
		SellerId:       sellerId,
		ClientId:       clientId,
		ContractNumber: contractNumber,
	}

	err = repositories.UpdateContract(contract)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, contract)
}

func DeleteContract(c *gin.Context) {

	id_param, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusBadRequest, "Bad parameter: id")
		return
	}

	id, err := strconv.Atoi(id_param)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: id")
		return
	}

	err = repositories.DeleteContractById(id)
	if err != nil {
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	c.JSON(http.StatusOK, true)
}
