package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"net/http"
	"strconv"
	"uplatforma/app"
	"uplatforma/models"
	"uplatforma/repositories"
)

func Purchase(c *gin.Context) {

	contractNumberParam := c.PostForm("contract_number")
	if contractNumberParam == "" {
		c.JSON(http.StatusBadRequest, "Invalid parameter: contract_number")
		return
	}
	contractNumber, err := strconv.Atoi(contractNumberParam)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: contract_number")
		return
	}

	creditsSpentParam := c.PostForm("credits_spent")
	if creditsSpentParam == "" {
		c.JSON(http.StatusBadRequest, "Require parameter: credits_spent")
		return
	}
	creditsSpent, err := decimal.NewFromString(creditsSpentParam)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid parameter: credits_spent")
		return
	}

	contract, err := repositories.GetContractByContractNumber(contractNumber)
	if err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, "Invalid parameter: Contract not found")
		return
	}
	if err != nil {
		c.JSON(http.StatusInternalServerError, "Undefined error")
		return
	}

	if contract.CreditsInitiallyUsed.Sub(*contract.CreditsUsedTotal).Sub(creditsSpent).Cmp(decimal.Zero) < 1 {
		c.JSON(http.StatusBadRequest, "Not enough credits")
		return
	}

	purchase := models.Purchase{
		ContractId:   contract.ID,
		CreditsSpent: creditsSpent,
	}

	tx := app.DB.Begin()
	err = repositories.UpdatePurchase(tx, purchase)
	if err != nil {
		tx.Rollback()
		ResponseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, ResponseError)
		return
	}

	tx.Commit()

	c.JSON(http.StatusOK, purchase)
}
