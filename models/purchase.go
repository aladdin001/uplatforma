package models

import (
	"github.com/shopspring/decimal"
	"time"
)

type Purchase struct {
	ID           int
	ContractId   int
	PurchaseTime time.Time
	CreditsSpent decimal.Decimal
}
