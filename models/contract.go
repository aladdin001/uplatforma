package models

import (
	"github.com/shopspring/decimal"
	"time"
)

type Contract struct {
	ID                   int
	SellerId             int
	ClientId             int
	ContractNumber       string
	DateSigned           time.Time
	ValidTill            *time.Time
	CreditsInitiallyUsed decimal.Decimal
	CreditsUsedTotal     *decimal.Decimal
}
