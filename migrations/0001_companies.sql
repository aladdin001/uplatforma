
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

create table companies
(
  id                serial       not null
    constraint companies_pk
    primary key,
  name              varchar(256) not null,
  registration_code varchar(256) not null
);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

drop table companies;