
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

create table purchases
(
  id            serial  not null
    constraint purchase_pk
    primary key,
  contract_id   integer not null
    constraint purchases_contracts_id_fk
    references contracts,
  purchase_time timestamp(0) default now(),
  credits_spent numeric
);

ALTER TABLE public.purchases
ADD CONSTRAINT purchases_contracts_id_fk
FOREIGN KEY (contract_id) REFERENCES public.contracts (id);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

drop constraint purchases_contracts_id_fk;

drop table purchases;