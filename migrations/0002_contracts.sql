
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

create table contracts
(
  id                     serial       not null
    constraint contract_pk
    primary key,
  seller_id              integer      not null
    constraint contracts_companies_id_fk
    references companies,
  client_id              integer      not null,
  contract_number        varchar(256) not null,
  date_signed            timestamp(0) default now(),
  valid_till             timestamp(0),
  credits_initially_used numeric
);

CREATE INDEX contracts_credits_initially_used_index ON public.contracts (credits_initially_used);

ALTER TABLE public.contracts
ADD CONSTRAINT contracts_companies_id_fk
FOREIGN KEY (seller_id) REFERENCES public.companies (id);

ALTER TABLE public.contracts
ADD CONSTRAINT contracts_companies_id_fk_2
FOREIGN KEY (client_id) REFERENCES public.companies (id);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

drop constraint contracts_companies_id_fk;
drop constraint contracts_companies_id_fk_2
drop table contracts;