package repositories

import (
	"github.com/jinzhu/gorm"
	"uplatforma/models"
)

func UpdatePurchase(tx *gorm.DB, purchase models.Purchase) (err error) {
	err = tx.Save(&purchase).Error
	if err != nil {
		return
	}

	err = tx.Exec("UPDATE contracts SET credits_initially_used = credits_initially_used - ? WHERE id=?", purchase.CreditsSpent, purchase.ContractId).Error
	return
}
