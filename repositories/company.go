package repositories

import (
	"uplatforma/app"
	"uplatforma/models"
)

func GetCompaniesList(limit, offset int) (companies []models.Company, err error) {
	err = app.DB.Model(models.Company{}).
		Order("name").
		Limit(limit).Offset(offset).
		Scan(&companies).Error
	return
}

func GetCompanyById(id int) (company models.Company, err error) {
	err = app.DB.Model(models.Company{}).
		Where("id = ?", id).Row().
		Scan(&company.ID, &company.Name, &company.RegistrationCode)
	return
}

func UpdateCompany(company models.Company) (err error) {
	err = app.DB.Save(&company).Error
	return
}

func DeleteCompanyById(id int) (err error) {
	err = app.DB.Delete(&models.Company{ID: id}).Error
	return
}
