package repositories

import (
	"uplatforma/app"
	"uplatforma/models"
)

func GetContractList(limit, offset int) (contracts []models.Contract, err error) {
	err = app.DB.Model(models.Contract{}).
		Limit(limit).Offset(offset).
		Scan(&contracts).Error
	return
}

func GetContractById(id int) (contract models.Contract, err error) {
	err = app.DB.Model(models.Contract{}).
		Where("id = ?", id).Row().
		Scan(&contract.ID, &contract.ClientId, &contract.SellerId, &contract.ContractNumber, &contract.DateSigned)
	return
}

func UpdateContract(company models.Contract) (err error) {
	err = app.DB.Save(&company).Error
	return
}

func DeleteContractById(id int) (err error) {
	err = app.DB.Delete(&models.Contract{ID: id}).Error
	return
}

func GetContractByContractNumber(contractNumber int) (contract models.Contract, err error) {
	err = app.DB.Raw(`SELECT 
				c.id, c.credits_initially_used, 
				(SELECT coalesce(SUM(credits_spent), 0) AS credits_spent FROM purchases WHERE contract_id = c.id) 
			FROM
				contracts c
			WHERE
				c.contract_number = ?`, contractNumber).Row().
		Scan(&contract.ID, &contract.CreditsInitiallyUsed, &contract.CreditsUsedTotal)
	return
}
